﻿using UnityEngine;

public class BossBehavior : MonoBehaviour
{
    public GameObject Firewall;
    public GameObject ManaItem;
    public GameObject SpawnedMinion;

    private int _pattern;
    private float _remainingTime;
    private int _remainingFires;
    private float _reloadTime;
    private bool _fireDelta;

    public void Start()
    {
        _pattern = 0;
        _remainingTime = 1.0f;
    }

    public void Update()
    {
        switch (_pattern)
        {
            case 0:
            case 2:
            case 4:
                WaitAction();
                break;
            case 1:
                FirewallAction();
                break;
            case 3:
                ThrowManaAction();
                break;
            case 5:
                SpawnMinions();
                break;
        }
    }

    private void WaitAction()
    {
        _remainingTime -= Time.deltaTime;
        if (_remainingTime < 0)
        {
            _pattern += 1;
        }
    }

    private void FirewallAction()
    {
        if (_remainingFires == 0)
        {
            _remainingFires = 10;
        }
        _reloadTime -= Time.deltaTime;
        if (_reloadTime < 0)
        {
            _reloadTime = 1.5f;
            _remainingFires -= 1;
            _fireDelta = !_fireDelta;
            for (int i = 0; i < 6; ++i)
            {
                Instantiate(Firewall, new Vector2((i - 3) * 5.0f + (_fireDelta ? 2.5f : 0.0f), 6.0f), Quaternion.identity);
            }
            if (_remainingFires <= 0)
            {
                _reloadTime = 0.0f;
                _remainingTime = 5.0f;
                _pattern = 2;
            }
        }
    }

    private void ThrowManaAction()
    {
        for (int i = 0; i < 7; ++i)
        {
            GameObject mana = (GameObject)Instantiate(ManaItem, transform.position, Quaternion.identity);
            Rigidbody2D body = mana.AddComponent<Rigidbody2D>();
            body.drag = 2.0f;
            body.AddForce(new Vector2((i - 3) * 0.33f, -1.0f).normalized * Random.Range(380.0f, 680.0f));
        }
        Instantiate(SpawnedMinion, transform.position, Quaternion.identity);
        _remainingTime = 2.0f;
        _pattern = 4;
    }

    private void SpawnMinions()
    {
        if (_remainingFires == 0)
        {
            _remainingFires = 5;
        }
        _reloadTime -= Time.deltaTime;
        if (_reloadTime < 0)
        {
            _reloadTime = 1.0f;
            _remainingFires -= 1;
            Instantiate(SpawnedMinion, transform.position, Quaternion.identity);
            if (_remainingFires <= 0)
            {
                _reloadTime = 0.0f;
                _remainingTime = 5.0f;
                _pattern = 0;
            }
        }
    }
}
