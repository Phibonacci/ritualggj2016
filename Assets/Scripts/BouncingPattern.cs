﻿using UnityEngine;

namespace Assets.Scripts
{
    public class BouncingPattern : MonoBehaviour
    {
        public float Speed;
        private Rigidbody2D _rigidbody;
        private Vector2 _velocity;
		int countOfChilds;
		Vector2 tmp;
		bool isAllowedToMoveToStartPoint;

		GameObject[] randomPositions;

        public void Start()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _velocity = new Vector2(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f)).normalized;

			countOfChilds = GameObject.FindGameObjectWithTag ("EnemySpawner").transform.childCount;
			randomPositions = new GameObject[countOfChilds];
			for (int i = 0; i < 8; i++) {
				randomPositions[i] = GameObject.FindGameObjectWithTag("RandomMoveLocations").transform.GetChild (i).gameObject;
			}
			tmp = MoveToObject ();
			isAllowedToMoveToStartPoint = true;

        }

        public void Update()
        {
			if (Vector2.Distance (gameObject.transform.position, tmp) > 0.1f && isAllowedToMoveToStartPoint == true) {
				transform.position = Vector2.MoveTowards (transform.position, tmp, Time.deltaTime * 3.0f);
			} else {
				isAllowedToMoveToStartPoint = false;
				_rigidbody.position += _velocity * Speed * Time.deltaTime;  
			}
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.collider.tag == "Map" || collision.collider.tag == "Player")
            {
                // Heck, I don't know how to do proper bouncing, so here is a bunch of ifs.
                if (collision.contacts[0].point.x < transform.position.x
                    && _velocity.x < 0)
                {
                    // Left
                    _velocity.x = -_velocity.x;
                }
                else if (collision.contacts[0].point.x > transform.position.x
                    && _velocity.x > 0)
                {
                    // Right
                    _velocity.x = -_velocity.x;
                }
                else if (collision.contacts[0].point.y > transform.position.y
                    && _velocity.y > 0)
                {
                    // Up
                    _velocity.y = -_velocity.y;
                }
                else if (collision.contacts[0].point.y < transform.position.y
                    && _velocity.y < 0)
                {
                    // Down
                    _velocity.y = -_velocity.y;
                }
            }
        }

		Vector2 MoveToObject(){
			return randomPositions [RandomNumber ()].transform.position;
		}


		int RandomNumber(){
			return Random.Range (0, countOfChilds);
		}
    }
}
