﻿using UnityEngine;

public class BulletBehavior : MonoBehaviour
{
    public int Damage;

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            PlayerController player = collider.GetComponent<PlayerController>();
            player.UpdateLife(Damage);
            Destroy(gameObject);
        }
        else if (collider.tag == "Map")
        {
            Destroy(gameObject);
        }
        else if (collider.tag == "PartOfBarrier")
        {
            //other.gameObject.SetActive(false);
            Destroy(collider.gameObject);
            Destroy(gameObject);
        }
    }
}
