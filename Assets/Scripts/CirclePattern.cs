﻿using UnityEngine;

namespace Assets.Scripts
{
    public class CirclePattern : MonoBehaviour
    {
        public float Speed;

        private float _rotationSpeed;
        private Rigidbody2D _rigidbody;
        private Vector2 _destination;
        private float _angle;
        private float _radius;
        private Vector2 _center;

		int countOfChilds;
		Vector2 tmp;
		bool isAllowedToMoveToStartPoint;
		GameObject[] randomPositions;

        public void Start()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _radius = Random.Range(1.0f, 3.2f);
            _rotationSpeed = 180.0f;
            _center = new Vector2(Random.Range(-3.0f, 3.0f), Random.Range(-3.0f, 3.0f));

			countOfChilds = GameObject.FindGameObjectWithTag ("EnemySpawner").transform.childCount;
			randomPositions = new GameObject[countOfChilds];
			for (int i = 0; i < 8; i++) {
				randomPositions[i] = GameObject.FindGameObjectWithTag("RandomMoveLocations").transform.GetChild (i).gameObject;
			}
			tmp = MoveToObject ();
			isAllowedToMoveToStartPoint = true;
        }

        public void Update()
        {
			if (Vector2.Distance (gameObject.transform.position, tmp) > 0.1f && isAllowedToMoveToStartPoint == true) {
				_rigidbody.position = Vector2.MoveTowards (transform.position, tmp, Time.deltaTime * 3.0f);
			} else {
				isAllowedToMoveToStartPoint = false;
				_angle += _rotationSpeed * Time.deltaTime;
				_rotationSpeed -= Time.deltaTime * 10;
				_destination = _center + new Vector2 (Mathf.Sin (_angle * Mathf.PI / 180.0f), Mathf.Cos (_angle * Mathf.PI / 180.0f)) * _radius;

				float distance = Vector2.Distance (_rigidbody.position, _center);
				if (Mathf.Abs (distance - _radius) < 0.01) {
					_radius = Random.Range (0.5f, 3.3f);
					_rotationSpeed = 180.0f;
				}

				_rigidbody.position = Vector2.MoveTowards (_rigidbody.position, _destination, Speed * Time.deltaTime);
			}
        }

		Vector2 MoveToObject(){
			return randomPositions [RandomNumber ()].transform.position;
		}


		int RandomNumber(){
			return Random.Range (0, countOfChilds);
		}
    }
}
