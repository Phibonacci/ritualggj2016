﻿using UnityEngine;
using System.Collections;

public class DeathMenuScript : MonoBehaviour {

    public void QuitGame()
    {
        Application.Quit();
    }

    public void NewGame()
    {
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().NewGame();
    }
}
