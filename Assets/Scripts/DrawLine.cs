﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class DrawLine : MonoBehaviour
{
    private LineRenderer line;
    private bool isMousePressed;
    public List<Vector3> pointsList;
    private Vector3 mousePos;

    public List<GameObject> points;
    private List<int> points2;

    public List<GameObject> border;

    struct myLine
    {
        public Vector3 StartPoint;
        public Vector3 EndPoint;
    };
     
    void Awake()
    {
        
        line = gameObject.AddComponent<LineRenderer>();
        line.material = new Material(Shader.Find("Particles/Additive"));
        line.SetVertexCount(0);
        line.SetWidth(0.1f, 0.1f);
        line.SetColors(Color.green, Color.green);
        line.useWorldSpace = true;
        isMousePressed = false;
        pointsList = new List<Vector3>();
        points2 = new List<int>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isMousePressed = true;
            line.SetVertexCount(0);
            pointsList.RemoveRange(0, pointsList.Count);
            line.SetColors(Color.green, Color.green);
        }
        if (Input.GetMouseButtonUp(0))
        {
            isMousePressed = false;
        }

        if (isMousePressed)
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0;
            if (!pointsList.Contains(mousePos))
            {
                pointsList.Add(mousePos);
                line.SetVertexCount(pointsList.Count);
                line.SetPosition(pointsList.Count - 1, (Vector3)pointsList[pointsList.Count - 1]);
                if(isLineCollideWithBorder())
                {
                    line.SetColors(Color.red, Color.red);
                    isMousePressed = false;
                }

                if (isLineCollideWithPoint())
                {
                    if(points2.Count == 9)
                    {
                        if (points2[0] == 6 &&
                            points2[1] == 5 &&
                            points2[2] == 0 &&
                            points2[3] == 1 &&
                            points2[4] == 4 &&
                            points2[5] == 7 &&
                            points2[6] == 8 &&
                            points2[7] == 3 &&
                            points2[8] == 2)
                        {
                            line.SetColors(Color.red, Color.red);
                            isMousePressed = false;
                        }
                    }
                        
                }
            }
        }
    }

    private bool isLineCollideWithBorder()
    {
        if (pointsList.Count < 2)
            return false;

        for (int i = 0; i < border.Count; i++)
        {
            myLine currentLine;

            currentLine.StartPoint = (Vector3)pointsList[pointsList.Count - 2];
            currentLine.EndPoint = (Vector3)pointsList[pointsList.Count - 1];
            if (doesLineTouchPoint(border[i], currentLine))
            {
                return true;
            }
        }
        return false;
    }


    private bool isLineCollideWithPoint()
    {
        if (pointsList.Count < 2)
            return false;

        for (int i = 0; i < points.Count; i++)
        {
            myLine currentLine;

            currentLine.StartPoint = (Vector3)pointsList[pointsList.Count - 2];
            currentLine.EndPoint = (Vector3)pointsList[pointsList.Count - 1];
            if (points[i].activeSelf)
            { 
                if (doesLineTouchPoint(points[i], currentLine))
                {
                    points2.Add(i);
                    points[i].SetActive(false);
                    return true;
                }
            }
        }
        return false;
    }
    
    private bool checkPoints(GameObject g1, Vector3 pointA)
    {
        
        if (pointA.x <= (g1.transform.position.x + g1.transform.localScale.x / 2) &&
            pointA.x >= (g1.transform.position.x - g1.transform.localScale.x / 2) &&
            pointA.y <= (g1.transform.position.y + g1.transform.localScale.y / 2) &&
            pointA.y >= (g1.transform.position.y - g1.transform.localScale.y / 2))
        {
            return true;
        } else {
            return false;
        }
        
    }
    
    private bool doesLineTouchPoint(GameObject g1, myLine L2)
    {
        if (checkPoints(g1, L2.StartPoint) ||
            checkPoints(g1, L2.EndPoint))
        {
            return true;
        } else {
            return false;
        }
    }
}