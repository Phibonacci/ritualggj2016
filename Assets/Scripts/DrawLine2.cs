﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;

public class DrawLine2 : MonoBehaviour
{
    private LineRenderer line;
    private bool isMousePressed;
    public List<Vector3> pointsList;
    private Vector3 mousePos;

    private List<GameObject> points;
    private List<int> points2;

    public List<GameObject> border;

    public List<GameObject> spells;

    public int currentSpell = 1;

    private float timeLeft = 5.0f;

    private Vector3 startMouse;

    public float power = 10.0f;

    public GameObject barrierCircle;

    public GameObject barrierSymbol;

    public GameObject windEffect;

    public GameObject explosionEffect;

    public GameObject lightningEffect;

	public GameObject emyspwner;

    struct myLine
    {
        public Vector3 StartPoint;
        public Vector3 EndPoint;
    };
     
    void Awake()
    {

        points = new List<GameObject>();

        foreach (Transform child in spells[currentSpell-1].transform)
        {
            points.Add(child.gameObject);
        }

        

        line = gameObject.AddComponent<LineRenderer>();
        line.material = new Material(Shader.Find("Particles/Additive"));
        line.SetVertexCount(0);
        line.SetWidth(0.1f, 0.1f);
        line.SetColors(Color.green, Color.green);
        line.useWorldSpace = true;
        isMousePressed = false;
        pointsList = new List<Vector3>();
        points2 = new List<int>();
    }

    void Update()
    {
        OpenPanels panelsScript = GameObject.FindGameObjectWithTag("Panels").GetComponent<OpenPanels>();

        if (panelsScript.getIsPaused() == false)
        {
            float scroll = Input.GetAxis("Mouse ScrollWheel");

            if (scroll < 0f)
            {
                line.SetVertexCount(0);
                pointsList.RemoveRange(0, pointsList.Count);
                line.SetColors(Color.green, Color.green);

                points2.RemoveRange(0, points2.Count);


                for (int i = 0; i < points.Count; i++)
                {
                    points[i].SetActive(false);
                }

                currentSpell--;
                if (currentSpell <= 0)
                {
                    currentSpell = spells.Count;
                }
                //Debug.Log(currentSpell);

                points.RemoveRange(0, points.Count);

                foreach (Transform child in spells[currentSpell - 1].transform)
                {
                    Debug.Log(child.gameObject);
                    points.Add(child.gameObject);
                }

            }
            else if (scroll > 0f)
            {
                line.SetVertexCount(0);
                pointsList.RemoveRange(0, pointsList.Count);
                line.SetColors(Color.green, Color.green);

                points2.RemoveRange(0, points2.Count);


                for (int i = 0; i < points.Count; i++)
                {
                    points[i].SetActive(false);
                }

                currentSpell++;
                if (currentSpell > spells.Count)
                {
                    currentSpell = 1;
                }
                //Debug.Log(currentSpell);

                points.RemoveRange(0, points.Count);

                foreach (Transform child in spells[currentSpell - 1].transform)
                {
                    Debug.Log(child.gameObject);
                    points.Add(child.gameObject);
                }
            }

            if (Input.GetMouseButtonDown(0))
            {

                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePos.z = 0;





                GameObject spellImage = null;

                foreach (Transform child in spells[currentSpell - 1].transform)
                {
                    if (child.tag == "Image")
                    {
                        spellImage = child.gameObject;
                    }
                }




                BoxCollider2D spellCollider = spellImage.GetComponent<BoxCollider2D>();

                GameObject map = GameObject.FindGameObjectWithTag("Map");


                BoxCollider2D[] colliders = map.GetComponents<BoxCollider2D>();


                float maxWidth = 0f;
                float maxHeight = 0f;

                foreach (BoxCollider2D child in colliders)

                {
                    if (child.size.x > maxWidth)
                        maxWidth = child.size.x;

                    if (child.size.y > maxHeight)
                        maxHeight = child.size.y;
                }

                isMousePressed = true;
                line.SetVertexCount(0);
                pointsList.RemoveRange(0, pointsList.Count);
                line.SetColors(Color.green, Color.green);






                Debug.Log(maxWidth + "/" + maxHeight);
                //Debug.Log(mousePos.x - spellCollider.size.x / 2);
                //Debug.Log(mousePos.y + spellCollider.size.y / 2);
                //Debug.Log(mousePos.y - spellCollider.size.y / 2);

                float t1 = mousePos.x + spellCollider.size.x / 2.0f;
                float t2 = mousePos.x - spellCollider.size.x / 2.0f;
                float t3 = mousePos.y + spellCollider.size.y / 2.0f;
                float t4 = mousePos.y - spellCollider.size.y / 2.0f;

                if (currentSpell == 1)
                {
                    t1 += 0.3f;
                    t2 -= 0.25f;
                    t3 -= 0.25f;
                    t4 += 0.25f;
                }
                if (currentSpell == 2)
                {
                    t1 -= 0.5f;
                    t2 += 0.5f;
                    t3 -= 1.0f;
                    t4 += 1.0f;
                }
                if (currentSpell == 3)
                {
                    t1 += 0.3f;
                    t2 += 1f;
                    t3 += 0.145f;
                    t4 += 0.145f;
                }

                if (currentSpell == 4)
                {
                    t1 += 0.25f;
                    t2 -= 0.25f;
                    t3 += 0.75f;
                    t4 -= 0.75f;
                }

                Debug.Log(t1 + "/" + t2 + "/" + t3 + "/" + t4);

                if (t1 < maxWidth / 2 &&
                    t2 > -maxWidth / 2 &&
                    t3 < maxHeight / 2 &&
                    t4 > -maxHeight / 2)
                {
                    for (int i = 0; i < points.Count; i++)
                    {
                        points[i].SetActive(false);
                    }

                    Debug.Log(currentSpell);

                    PlayerController playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

                    playerScript.UpdateMana(-10);

                    isMousePressed = true;
                    line.SetVertexCount(0);
                    pointsList.RemoveRange(0, pointsList.Count);
                    line.SetColors(Color.green, Color.green);

                    startMouse = mousePos;

                    for (int i = 0; i < points.Count; i++)
                    {

                        points[i].SetActive(true);
                    }

                    spells[currentSpell - 1].transform.position = mousePos;

                    timeLeft = 5.0f;
                }



            }
            if (Input.GetMouseButtonUp(0))
            {
                clear();

            }

            if (isMousePressed)
            {         

                timeLeft -= Time.deltaTime;
                //Debug.Log(timeLeft);
                if (timeLeft <= 0)
                {
                    isMousePressed = false;
                    line.SetColors(Color.red, Color.red);
                }

                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePos.z = -2;
                if (!pointsList.Contains(mousePos))
                {
                    pointsList.Add(mousePos);
                    line.SetVertexCount(pointsList.Count);
                    line.SetPosition(pointsList.Count - 1, (Vector3)pointsList[pointsList.Count - 1]);
                    if (isLineCollideWithBorder())
                    {
                        line.SetColors(Color.red, Color.red);
                        isMousePressed = false;
                    }

                    if (isLineCollideWithPoint())
                    {
                    }

                    if (points2.Count == 9)
                    {
                        if (points2[0] == 4 &&
                            points2[1] == 1 &&
                            points2[2] == 0 &&
                            points2[3] == 5 &&
                            points2[4] == 6 &&
                            points2[5] == 7 &&
                            points2[6] == 8 &&
                            points2[7] == 3 &&
                            points2[8] == 2)
                        {

                            line.SetColors(Color.blue, Color.blue);
                            isMousePressed = false;

                            line.SetVertexCount(0);
                            pointsList.RemoveRange(0, pointsList.Count);
                            line.SetColors(Color.green, Color.green);

                            points2.RemoveRange(0, points2.Count);


                            for (int i = 0; i < points.Count; i++)
                            {
                                points[i].SetActive(false);
                            }

                            switch (currentSpell)
                            {
                                case 1:
                                    Debug.Log("Explosion!!!");
                                    explosion(startMouse);
                                    break;
                                case 2:
                                    Debug.Log("Barrier!!!");
                                    barrier(startMouse);
                                    break;
                                case 3:
                                    Debug.Log("Wind!!!");
                                    wind(startMouse);
                                    break;
                                case 4:
                                    Debug.Log("LastHope!!!");
                                    lastHope(startMouse);
                                    break;
                                default:

                                    break;
                            }
                        }
                    }
                }
            }
        }
    }

    private void barrier(Vector3 mPos)
    {

        StartCoroutine(createCircle(mPos));

        
    }

    public void clear()
    {
        isMousePressed = false;
        line.SetVertexCount(0);
        pointsList.RemoveRange(0, pointsList.Count);
        line.SetColors(Color.green, Color.green);

        points2.RemoveRange(0, points2.Count);


        for (int i = 0; i < points.Count; i++)
        {
            points[i].SetActive(false);
        }
    }

    IEnumerator createCircle(Vector3 mPos)
    {
        barrierCircle.transform.position = new Vector3(mPos.x, mPos.y, 0f);
        yield return new WaitForSeconds(2);
        Collider2D[] colliders = Physics2D.OverlapCircleAll(mPos, 1f);
        foreach (Collider2D hit in colliders)
        {
            if (hit.tag == "Player")
            {
                PlayerController playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
                playerScript.activateBarrier = true;
            }

        }
        barrierCircle.transform.position = new Vector3(mPos.x, mPos.y, 10f);
    }

    private void lastHope(Vector3 mPos)
    {
        Instantiate(lightningEffect, new Vector3(mPos.x, mPos.y, -2f), Quaternion.identity);

        Collider2D[] colliders = Physics2D.OverlapCircleAll(mPos, 0.25f);
        foreach (Collider2D hit in colliders)
        {
            if (hit.tag == "Enemy")
            {

                Destroy(hit.gameObject);
				emyspwner.GetComponent<EnemySpawnerScript> ().globalEnemyCount -= 3;

                EnemyBehavior enemy = hit.GetComponent<EnemyBehavior>();
                if (enemy != null)
                {
                    enemy.Kill();
                }
                else
                {
                    Destroy(hit.gameObject);
                }
            }

        }
    }

    private void explosion(Vector3 mPos)
    {

        Instantiate(explosionEffect, new Vector3(mPos.x, mPos.y, -2f), Quaternion.identity);

        Collider2D[] colliders = Physics2D.OverlapCircleAll(mPos, 1f);
        foreach (Collider2D hit in colliders)
        {
            if(hit.tag == "Enemy")
            {
                Destroy(hit.gameObject);
				emyspwner.GetComponent<EnemySpawnerScript> ().globalEnemyCount -= 3;

                EnemyBehavior enemy = hit.GetComponent<EnemyBehavior>();
                if (enemy != null)
                {
                    enemy.Kill();
                }
                else
                {
                    Destroy(hit.gameObject);
                }
            }
            
        }
    }

    private void wind(Vector3 mPos)
    {
        StartCoroutine(createWind(mPos));
    }

    IEnumerator createWind(Vector3 mPos)
    {
        windEffect.transform.position = new Vector3(mPos.x, mPos.y, -2f);
        

        Collider2D[] colliders = Physics2D.OverlapCircleAll(mPos, 5f);
        foreach (Collider2D hit in colliders)
        {
            //if (hit.tag == "Enemy")
            //{
            Rigidbody2D rb = hit.GetComponent<Rigidbody2D>();

            if (rb != null)
                AddWindForce(rb, power * 100, mPos, 5f);

            //}

        }
        yield return new WaitForSeconds(0.5f);
        windEffect.transform.position = new Vector3(0, 0, 10f);

        
    }

    public static void AddWindForce(Rigidbody2D body, float expForce, Vector3 expPosition, float expRadius)
    {
        Vector3 dir = (body.transform.position - expPosition);
        float calc = 1 - (dir.magnitude / expRadius);
        if (calc <= 0)
        {
            calc = 0;
        }
        Debug.Log("Add Force!!");
        body.AddForce(dir.normalized * expForce * calc);
    }

    private bool isLineCollideWithBorder()
    {
        if (pointsList.Count < 2)
            return false;

        for (int i = 0; i < border.Count; i++)
        {
            myLine currentLine;

            currentLine.StartPoint = (Vector3)pointsList[pointsList.Count - 2];
            currentLine.EndPoint = (Vector3)pointsList[pointsList.Count - 1];
            if (doesLineTouchPoint(border[i], currentLine))
            {
                return true;
            }
        }
        return false;
    }


    private bool isLineCollideWithPoint()
    {
        if (pointsList.Count < 2)
            return false;

        for (int i = 0; i < 9; i++)
        {
            myLine currentLine;

            currentLine.StartPoint = (Vector3)pointsList[pointsList.Count - 2];
            currentLine.EndPoint = (Vector3)pointsList[pointsList.Count - 1];
            if (points[i].activeSelf)
            { 
                if (doesLineTouchPoint(points[i], currentLine))
                {
                    points2.Add(i);
                    points[i].SetActive(false);
                    return true;
                }
            }
        }
        return false;
    }
    
    private bool checkPoints(GameObject g1, Vector3 pointA)
    {
        
        if (pointA.x <= (g1.transform.position.x + g1.transform.localScale.x / 2) &&
            pointA.x >= (g1.transform.position.x - g1.transform.localScale.x / 2) &&
            pointA.y <= (g1.transform.position.y + g1.transform.localScale.y / 2) &&
            pointA.y >= (g1.transform.position.y - g1.transform.localScale.y / 2))
        {
            return true;
        } else {
            return false;
        }
        
    }
    
    private bool doesLineTouchPoint(GameObject g1, myLine L2)
    {
        if (checkPoints(g1, L2.StartPoint) ||
            checkPoints(g1, L2.EndPoint))
        {
            return true;
        } else {
            return false;
        }
    }
}