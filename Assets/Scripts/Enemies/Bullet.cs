﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    private Transform shotDirection;
    private float bulletSpeed;
    private Vector3 move;
	public AudioSource asBulletHitSound;

    void Start()
    {
        shotDirection = transform;        
    }

    void Update()
    {
        if(move == Vector3.zero)
            move = new Vector2(0, -1);
        shotDirection.Translate(move * bulletSpeed * Time.deltaTime);
        Destroy(gameObject, 5f);
    }

    public void SetDirection(Vector3 x)
    {
        move = x;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().TakeDamage(10);
			asBulletHitSound.Play ();
        }
        else if (other.tag == "PartOfBarrier")
        {
            //other.gameObject.SetActive(false);
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }

    public void SetBulletSpeed(float speed)
    {
        bulletSpeed = speed;
    }


	
}
