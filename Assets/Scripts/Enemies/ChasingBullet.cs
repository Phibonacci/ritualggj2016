﻿using UnityEngine;
using System.Collections;

public class ChasingBullet : MonoBehaviour {
    Rigidbody2D rb;
    Vector3 direction;
    GameObject player;
    float bulletSpeed;
	public AudioSource asBulletHitSound;
	// Use this for initialization
	void Start () {
        bulletSpeed = 5;
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody2D>();
        direction = (player.transform.position - transform.position);
        direction = direction.normalized;
    }
	
	// Update is called once per frame
	void Update ()
    {
        rb.velocity = (direction * bulletSpeed);
        Destroy(gameObject, 5f);
    }

    public void setBulletSpeed(float f)
    {
        bulletSpeed = f;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().TakeDamage(30);
            Destroy(gameObject);
        }
        else if(other.tag == "PartOfBarrier")
        {
            //other.gameObject.SetActive(false);
            Destroy(other.gameObject);
            Destroy(gameObject);
			asBulletHitSound.Play ();
        }
    }
}
