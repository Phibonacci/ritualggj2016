﻿using UnityEngine;
using System.Collections;

public class ChasingBulletShooter : MonoBehaviour {

    public GameObject bullet;
    public Transform shotSpawn;
    public float shotSpeed;

    private float idleTime;
    public float timeTillShot = 2f;

    void Start()
    {
        idleTime = Time.time + timeTillShot;
    }
    void Update()
    {
        if (Time.time > idleTime  && GameObject.FindGameObjectsWithTag("Player").Length > 0)
        {
            GameObject go = Instantiate(bullet, new Vector3(shotSpawn.transform.position.x, shotSpawn.transform.position.y, -1), Quaternion.identity) as GameObject;
            go.GetComponent<ChasingBullet>().setBulletSpeed(shotSpeed);
            idleTime = Time.time + timeTillShot;
        }
    }
}
