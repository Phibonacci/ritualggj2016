﻿using UnityEngine;
using System.Collections;

public class EnemyThreeShot : MonoBehaviour {

    Rigidbody2D rb;
    public float speed;
    public GameObject bullet;
    public Transform shotSpawn;
    public float shotSpeed;

    private float idleTime;
    private float timeBetweenShots;
    public float timeTillShot = 2f;
    public float fireRate = 0.2f;
    private int shotCount;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        idleTime = Time.time + timeTillShot;
        timeBetweenShots = Time.time + fireRate;
        shotCount = 0;
    }
    void Update()
    {
        Vector2 movement = new Vector2(0,0).normalized; // Vector2.zero;
        rb.position += movement * speed * Time.deltaTime;

        if (Time.time > idleTime)
            if (shotCount < 3)
            {
                if(Time.time > timeBetweenShots)
                {
                    GameObject go = Instantiate(bullet, new Vector3(shotSpawn.transform.position.x, shotSpawn.transform.position.y, - 1), Quaternion.identity) as GameObject;
                    go.GetComponent<Bullet>().SetBulletSpeed(shotSpeed);
                    go.GetComponent<Bullet>().SetDirection(GetDirection());
                    shotCount++;
                    timeBetweenShots = Time.time + fireRate;
                }
                
            }
            else
            {
                idleTime = Time.time + timeTillShot;
                shotCount = 0;
            }
    }
    public Vector2 GetDirection()
    {
        Vector2 x = Vector2.zero;
        int rdm = Mathf.RoundToInt(Random.Range(1, 8));
        switch (rdm)
        {
            case 1:
                x = new Vector2(0, 1);
                break;
            case 2:
                x = new Vector2(0, -1);
                break;
            case 3:
                x = new Vector2(1, 0);
                break;
            case 4:
                x = new Vector2(-1, 0);
                break;
            case 5:
                x = new Vector2(-1, -1);
                break;
            case 6:
                x = new Vector2(1, -1);
                break;
            case 7:
                x = new Vector2(-1, 1);
                break;
            case 8:
                x = new Vector2(1, 1);
                break;
        }
        return x;

    }

}
