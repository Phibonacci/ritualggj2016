﻿using UnityEngine;
using System.Collections;

public class EnemyWallShooter : MonoBehaviour {

    public GameObject bullet;
    public Transform shotSpawn;
    public float shotSpeed;

    private float idleTime;
    public float timeTillShot = 2f;

    void Start()
    {
        shotSpawn = transform.FindChild("ShotSpawn").transform;
        idleTime = Time.time + timeTillShot;
    }
    void Update()
    {
        //Vector2 movement = new Vector2(0, 0).normalized; // Vector2.zero;

        if (Time.time > idleTime && GameObject.FindGameObjectsWithTag("Player").Length > 0)
        {
            Instantiate(bullet, new Vector3(shotSpawn.transform.position.x, shotSpawn.transform.position.y,-1), Quaternion.identity);
            idleTime = Time.time + timeTillShot;
        }
    }
}
