﻿using UnityEngine;
using System.Collections;

public class FireWallBullet : MonoBehaviour {

    // unused?
    //private Transform shotDirection;
    private float bulletSpeed;
    private Vector3 move;
    private GameObject player;
    private Rigidbody2D rb;
    private float differenceY;
    private float differenceX;
    private float birth;
    private float timeToMaxSize;
    private bool isInvoking;
	public AudioSource asBulletHitSound;

    void Start()
    {
        InvokeRepeating("ScaleWall", 0, 0.05f);
        birth = Time.time;
        timeToMaxSize = 3f;
        rb = GetComponent<Rigidbody2D>();
        bulletSpeed = 2;
        //shotDirection = transform;
        player = GameObject.FindGameObjectWithTag("Player");
        gameObject.transform.localScale = new Vector3(0.1f,0.1f);
        if (transform.position.x > player.transform.position.x) 
             differenceX = transform.position.x - player.transform.position.x;
        else
            differenceX = player.transform.position.x - transform.position.x;

        if (transform.position.y > player.transform.position.y)
            differenceY = transform.position.y - player.transform.position.y;
        else
            differenceY = player.transform.position.y - transform.position.y;
        
        if (differenceY > differenceX)
        {
            if (transform.position.y > player.transform.position.y)
            {
                move = new Vector2(0, -1);

            }
            else
            {
                move = new Vector2(0, 1);
                transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 180));
            }
        }
        else
        {
            if (transform.position.x > player.transform.position.x)
            {
                move = new Vector2(-1, 0);
                transform.localRotation = Quaternion.Euler(new Vector3(0,0, -90));
            }
            else
            {
                move = new Vector2(1, 0);
                transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 90));
            }
                
        }
    }
    void Update()
    {
        if (Time.time > (birth + timeToMaxSize) && isInvoking == true)
        {
            CancelInvoke();
            isInvoking = false;
            Debug.Log("Canceled " + isInvoking);
        }
        rb.velocity = move.normalized * bulletSpeed;
        Destroy(gameObject, 5f);
        //shotDirection.Translate(move * bulletSpeed * Time.deltaTime);

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().TakeDamage(40);
			asBulletHitSound.Play ();
        }
        else if (other.tag == "PartOfBarrier")
        {
            //other.gameObject.SetActive(false);
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }

    public void SetBulletSpeed(float speed)
    {
        bulletSpeed = speed;
    }
    public void ScaleWall()
    {
        isInvoking = true;
        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x + 0.0075f, gameObject.transform.localScale.y + 0.005f);
    }
}
