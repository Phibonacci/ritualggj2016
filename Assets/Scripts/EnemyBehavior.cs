﻿using UnityEngine;

namespace Assets.Scripts
{
    public class EnemyBehavior : MonoBehaviour
    {
        public int Damage;
		public AudioSource asHit;
        public float DropProbability;
        public GameObject[] Drops;

        public void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.collider.tag == "Player")
            {
                PlayerController player = collision.collider.GetComponent<PlayerController>();
                player.TakeDamage(Damage);
				asHit.Play ();
            }
            else if (collision.collider.tag == "PartOfBarrier")
            {
                //other.gameObject.SetActive(false);
                Destroy(collision.collider.gameObject);
                Destroy(gameObject);
            }
        }

        public void Kill()
        {
            if (Random.value < DropProbability)
            {
                Instantiate(Drops[Random.Range(0, Drops.Length)], transform.position, Quaternion.identity);
            }
        }
    }
}
