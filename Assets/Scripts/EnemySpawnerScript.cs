﻿using UnityEngine;
using System.Collections;

public class EnemySpawnerScript : MonoBehaviour {

	GameObject[] spawner;
	GameObject[] enemies;
	public GameObject enemyA;
	public GameObject enemyB;
	public GameObject enemyC;
	GameObject enemyToSpawn;
	//Is set in Editor!
	public int globalEnemyCount;
	Vector3 tmp;
	bool isAllowedToSpawnEnemy;
	public int maxEnemys;
	int enemyCCount;
	int enemyBCount;
	bool cIsAllowedToSpawn;
	bool bIsAllowedToSpawn;
	float spawnTime;
//	float waitForNextRitual;
	int maxCountEnemyC;
	int maxCountEnemyB;

	// Use this for initialization
	void Start () {
		spawner = new GameObject[8];
		for (int i = 0; i < 8; i++) {
			spawner [i] = GameObject.FindGameObjectWithTag ("EnemySpawner").transform.GetChild (i).gameObject;
		}
		globalEnemyCount = 0;
		tmp = Vector3.zero;
		enemies = new GameObject[3];
		enemies[0] = enemyA;
		enemies[1] = enemyB;
		enemies[2] = enemyC;
		isAllowedToSpawnEnemy = true;
		maxEnemys = 15;
		enemyCCount = 0;
		maxCountEnemyC = 3;
		enemyBCount = 0;
		maxCountEnemyC = 5;
		cIsAllowedToSpawn = true;
		bIsAllowedToSpawn = true;
		spawnTime = 5f;
		//waitForNextRitual = 180f;
		//StartCoroutine (IncreaseMaxEnemysAndTheirSpawntime ());
	}
	
	// Update is called once per frame
	void Update () {
		SpawnEnemyTimer ();
		SpawnEnemy ();
	}

	int RandomNumber(){
		return Random.Range (0, 8);
	}

	int RandomNumberForAllEnemyArray(){
		return Random.Range (0, 3);
	}

	int RandomNumberForTwoEnemyArray(){
		return Random.Range (0, 2);
	}

	void SpawnEnemy(){
		if(isAllowedToSpawnEnemy == true){
			StartCoroutine (SpawnEnemyTimer ());
			if (globalEnemyCount <= maxEnemys) {
				if (globalEnemyCount == maxEnemys - 1) {
					tmp = spawner [RandomNumber ()].transform.position;
					int i = 0;
					InstantiateEnemy (tmp, i);
				} else if (globalEnemyCount < maxEnemys) {
					tmp = spawner [RandomNumber ()].transform.position;
					int i = RandomNumberForAllEnemyArray ();
					InstantiateEnemy (tmp, i);
				} else if (globalEnemyCount == maxEnemys-2) {
					tmp = spawner [RandomNumber ()].transform.position;
					int i = RandomNumberForTwoEnemyArray ();
					InstantiateEnemy (tmp, i);
				} 
			}
		}			
	}

	void InstantiateEnemy(Vector3 tmp, int enemyToSpawn){
		if (enemyToSpawn == 2 && cIsAllowedToSpawn == true) {
			Instantiate (enemies [enemyToSpawn], tmp, Quaternion.identity);
			enemyCCount += 1;
			globalEnemyCount += (enemyToSpawn + 1);
			if (enemyCCount == maxCountEnemyC) {
				cIsAllowedToSpawn = false;
			}
		}else if (enemyToSpawn == 1 && bIsAllowedToSpawn == true) {
			Instantiate (enemies [enemyToSpawn], tmp, Quaternion.identity);
			enemyBCount += 1;
			//Debug.Log (enemyBCount);
			globalEnemyCount += (enemyToSpawn + 1);
			if (enemyBCount == maxCountEnemyB) {
				bIsAllowedToSpawn = false;
			}
		}else if (enemyToSpawn == 0) {
			Instantiate (enemies [enemyToSpawn], tmp, Quaternion.identity);
			globalEnemyCount += (enemyToSpawn + 1);
		}
	}           

	IEnumerator SpawnEnemyTimer(){
		isAllowedToSpawnEnemy = false;
		yield return new WaitForSeconds (spawnTime);
		isAllowedToSpawnEnemy = true;
	}

	/*IEnumerator IncreaseMaxEnemysAndTheirSpawntime(){
		yield return new WaitForSeconds (waitForNextRitual);
		spawnTime = spawnTime / 2;
		maxEnemys = 25;
		maxCountEnemyB = 10;
		maxCountEnemyC = 6;
	}*/
}
