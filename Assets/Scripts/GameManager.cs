﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        gameObject.tag = "GameManager";
    }

    public int GetCurrentStage()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

    public void NextLevel()
    {
        SceneManager.LoadScene(GetCurrentStage() + 1);
    }

    public void NewGame()
    {
        SceneManager.LoadScene(1);
    }

    public void Death()
    {
        SceneManager.LoadScene("DeathScene");
    }
}
