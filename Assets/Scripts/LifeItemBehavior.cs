﻿using UnityEngine;

public class LifeItemBehavior : MonoBehaviour
{
    public int Amount;
	public AudioSource lifeSound;

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            PlayerController player = collider.GetComponent<PlayerController>();
            player.UpdateLife(Amount);
			lifeSound.Play ();
            Destroy(gameObject);
        }
    }
}
