﻿using UnityEngine;

namespace Assets.Scripts
{
    public class LinePattern : MonoBehaviour
    {
        public float Speed;

        private Rigidbody2D _rigidbody;
        private Vector2 _velocity;

		int countOfChilds;
		Vector2 tmp;
		bool isAllowedToMoveToStartPoint;
		GameObject[] randomPositions;

        public void Start()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            switch (Random.Range(0, 4))
            {
                case 0:
                    _velocity = Vector2.up;
                    break;
                case 1:
                    _velocity = Vector2.down;
                    break;
                case 2:
                    _velocity = Vector2.left;
                    break;
                case 3:
                    _velocity = Vector2.right;
                    break;
            }

			countOfChilds = GameObject.FindGameObjectWithTag ("EnemySpawner").transform.childCount;
			randomPositions = new GameObject[countOfChilds];
			for (int i = 0; i < 8; i++) {
				randomPositions[i] = GameObject.FindGameObjectWithTag("RandomMoveLocations").transform.GetChild (i).gameObject;
			}
			tmp = MoveToObject ();
			isAllowedToMoveToStartPoint = true;
        }

        public void Update()
        {
			if (Vector2.Distance (gameObject.transform.position, tmp) > 0.1f && isAllowedToMoveToStartPoint == true) {
				transform.position = Vector2.MoveTowards (transform.position, tmp, Time.deltaTime * 3.0f);
			} else {
				isAllowedToMoveToStartPoint = false;
				_rigidbody.position += _velocity * Speed * Time.deltaTime;
			}
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.collider.tag == "Map" || collision.collider.tag == "Player")
            {
                _velocity = -_velocity;
            }
        }

		Vector2 MoveToObject(){
			return randomPositions [RandomNumber ()].transform.position;
		}


		int RandomNumber(){
			return Random.Range (0, countOfChilds);
		}
    }
}
