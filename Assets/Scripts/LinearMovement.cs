﻿using UnityEngine;

public class LinearMovement : MonoBehaviour
{
    public Vector3 TranslationSpeed;
    public Vector3 RotationSpeed;

    public void Update()
    {
        transform.position += TranslationSpeed;
        transform.localEulerAngles += RotationSpeed;
    }
}
