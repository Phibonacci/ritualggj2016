﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainSceneScript : MonoBehaviour {

	public GameObject StartPanel;
	public GameObject CreditPanel;

	public void CloseGame(){
		Application.Quit ();
	}

	public void ShowCredits(){
		StartPanel.SetActive (false);
		CreditPanel.SetActive (true);
	}

	public void CloseCredits(){
		StartPanel.SetActive (true);
		CreditPanel.SetActive (false);
	}

	public void LoadGame(){
		SceneManager.LoadScene ("Level1");
	}
}
