﻿using UnityEngine;

public class ManaItemBehavior : MonoBehaviour
{
    public int Amount;
	public AudioSource manaSound;

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            PlayerController player = collider.GetComponent<PlayerController>();
            player.UpdateMana(Amount);
			manaSound.Play ();
            Destroy(gameObject);
        }
    }
}
