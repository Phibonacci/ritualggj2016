﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MasterVolumeScript : MonoBehaviour {

	public Slider slider;
	public AudioSource asBackground;

	void Start(){
		AudioListener.volume = slider.value;
		PlayMusic ();
	}


	// Update is called once per frame
	void Update () {

	}


	public void ChangeGameVolume(){
		AudioListener.volume = slider.value;
		PlayMusic ();
	}

	void PlayMusic(){
		if (slider.value <= slider.minValue) {
			asBackground.Pause ();
		} else if(!asBackground.isPlaying){
			asBackground.UnPause ();
		}
	}
}
