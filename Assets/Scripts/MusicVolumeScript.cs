﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MusicVolumeScript : MonoBehaviour {

	public Slider slider;
	public AudioSource asBackground;

	void Start(){
		asBackground.volume = slider.value;
	}

	public void ChangeBackgroundMusicVolume(){
		asBackground.volume = slider.value;
		PlayMusic ();
	}

	void PlayMusic(){
		if (slider.value <= slider.minValue) {
			asBackground.Pause ();
		} else if(!asBackground.isPlaying){
			asBackground.UnPause ();
		}
	}
}
