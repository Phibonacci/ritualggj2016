﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OpenPanels : MonoBehaviour {

	bool isPaused;
	bool allowToUseEscape;
	public AudioSource asBackground;
	public GameObject OptionButtonPanel;
	public GameObject ControllPanel;
	public GameObject OptionPanel;

	// Use this for initialization
	void Start () {
		isPaused = false;
		allowToUseEscape = true;
		/*OptionButtonPanel = GameObject.FindGameObjectWithTag ("OptionButtonPanel");
		ControllPanel = GameObject.FindGameObjectWithTag ("ControllPanel");
		OptionPanel = GameObject.FindGameObjectWithTag ("OptionPanel");

		OptionButtonPanel.SetActive (false);
		ControllPanel.SetActive (false);
		OptionPanel.SetActive (false);*/
	}
	
	// Update is called once per frame
	void Update () {
		OpenOptionButtonPanel ();
	}

	void OpenOptionButtonPanel(){
		if (allowToUseEscape == true) {
			if (Input.GetKeyDown (KeyCode.Escape)) {
				if (isPaused == false) {
					Time.timeScale = 0;
					asBackground.Pause ();
					isPaused = true;
					OptionButtonPanel.SetActive (true);
                    DrawLine2 drawLineScript = GameObject.FindGameObjectWithTag("SpellPlayer").GetComponent<DrawLine2>();
                    drawLineScript.clear();
                } else {
					CloseOptionButtonPanel ();
				}
			}
		}
	}

    public bool getIsPaused()
    {
        return isPaused;
    }

	public void CloseOptionButtonPanel(){
		Time.timeScale = 1;
		asBackground.UnPause ();
		isPaused = false;
		OptionButtonPanel.SetActive (false);
	}

	public void OpenOptionPanel(){
		OptionButtonPanel.SetActive (false);
		allowToUseEscape = false;
		OptionPanel.SetActive (true);

	}

	public void CloseOptionPanel(){
		OptionButtonPanel.SetActive (true);
		allowToUseEscape = true;
		OptionPanel.SetActive (false);
		asBackground.Pause ();
	}

	public void OpenControllPanel(){
		OptionButtonPanel.SetActive (false);
		allowToUseEscape = false;
		ControllPanel.SetActive (true);
	}

	public void CloseControllPanel(){
		OptionButtonPanel.SetActive (true);
		allowToUseEscape = true;
		ControllPanel.SetActive (false);
	}

	public void QuitGame(){
		Application.Quit ();
	}
}
