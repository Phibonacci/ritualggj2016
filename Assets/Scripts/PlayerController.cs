﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public int Speed;
    public int InitialLife;
    public int InitialMana;

    public int Life { get; private set; }
    public int Mana { get; private set; }

    private Rigidbody2D _rigidbody;
    private SpriteRenderer _sprite;
    private float _invulnerabilityTime;
    private GameObject gameManager;

    public bool activateBarrier;

    private bool activeBarrier;

    private float count = 0;


    public GameObject barrier;
	public AudioSource asDestroy;

    public void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _sprite = GetComponent<SpriteRenderer>();
        UpdateLife(InitialLife);
        UpdateMana(InitialMana);
        activateBarrier = false;
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        activeBarrier = false;
    }

    public void Update()
    {
        Vector2 movement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")).normalized;

        //Debug.Log("Count: " + count + " .Time:" + Time.deltaTime);

        if(movement.x == 0 && movement.y == 0)
        {
            count += Time.deltaTime;            
        }

        if(count >= 1)
        {
            UpdateMana(5);
            count = 0;
        }

        _rigidbody.position += movement * Speed * Time.deltaTime;
        _rigidbody.velocity = Vector2.zero;
        if (movement.x < 0)
        {
            _sprite.flipX = true;
        }
        else if (movement.x > 0)
        {
            _sprite.flipX = false;
        }
        if (_invulnerabilityTime > 0)
        {
            _invulnerabilityTime -= Time.deltaTime;
            if (_invulnerabilityTime > 0 && (Time.time % 0.10f) < 0.05f)
            {
                _sprite.color = new Color(0, 0, 0, 0);
            }
            else
            {
                _sprite.color = Color.white;
            }
        }

        if(activateBarrier)
        {
            if(activeBarrier)
            {
                foreach (Transform child in transform)
                {
                    Destroy(child.gameObject);
                }
            }
            GameObject go = Instantiate(barrier, transform.position, Quaternion.identity) as GameObject;
            go.transform.parent = transform;
            activateBarrier = false;
            activeBarrier = true;
        }


    }

    public void UpdateLife(int delta)
    {
        Life += delta;
        if (Life > 100) Life = 100;
        if (Life < 0) Life = 0;
        if (Life == 0)
        {
            Destroy(gameObject);
			asDestroy.Play ();
            gameManager.GetComponent<GameManager>().Death();
        }
    }

    public void UpdateMana(int delta)
    {
        Mana += delta;
        if (Mana > 100) Mana = 100;
        if (Mana < 0) Mana = 0;
    }
    
    public void TakeDamage(int damage)
    {
        if (_invulnerabilityTime > 0)
        {
            return;
        }
        UpdateLife(-damage);
        _invulnerabilityTime = 1.0f;
    }
}
