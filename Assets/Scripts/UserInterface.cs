﻿using UnityEngine;
using UnityEngine.UI;

public class UserInterface : MonoBehaviour {

    public Image HealthSlider;
    public Image ManaSlider;
    public Image ActiveSpellImage;

    public Sprite[] HealthSprites;
    public Sprite[] ManaSprites;
    public Sprite[] SpellSprites;

    private PlayerController player;
    private DrawLine2 spells;

    void Start()
    {
        player = GetComponent<PlayerController>();
        spells = GameObject.FindGameObjectWithTag("SpellPlayer").GetComponent<DrawLine2>();
    }

    void Update()
    {
        int index = 10 - player.Life / 10;
        if (index < 0) index = 0;
        if (index > 9) index = 9;
        HealthSlider.sprite = HealthSprites[index];

        index = 10 - player.Mana / 10;
        if (index < 0) index = 0;
        if (index > 9) index = 9;
        ManaSlider.sprite = ManaSprites[index];

        if (spells.currentSpell >= 1 && spells.currentSpell <= SpellSprites.Length)
        {
            ActiveSpellImage.sprite = SpellSprites[spells.currentSpell - 1];
        }
    }
}
