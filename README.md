#Ritual - Global Game Jam 2016

#Team

 * Jean Fauquenot
 * Fabian Haupt
 * Paul-Maxime Le Duc
 * Michael Pollak
 * Lannard Dirk
 * Sebastian Kersting
 * Aziza Kireyeva
 * Gerard Cunningham

#Game Design Document

[Link to Document on Google Docs.](https://docs.google.com/document/d/1guTHiBuaqQsO_zjzfFZdv2yHJaLPpPfZLbuKuYvlRGo/edit#)